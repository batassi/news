module.exports = function(grunt){
    //show elapsed time at the end
    require('time-grunt')(grunt);

    //load all grunt tasks
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        
        //bower install
        bowercopy: {
            options: {
                srcPrefix: 'bower_components',
                clean: true
            },
            js: {
                files: {
                    'libs/js/jquery.js': 'jquery/dist/jquery.js',
                    'libs/js/bootstrap.js': 'bootstrap/dist/js/bootstrap.js'
                }
            },
            css: {
                files: {
                    'libs/css/bootstrap.css': 'bootstrap/dist/css/bootstrap.css',
                    'libs/css/font-awesome.css': 'font-awesome/css/all.css'
                }
            },
            font: {
                files: {
                    'public/webfonts': ['font-awesome/webfonts']
                }
            }
        },

        //concat
        concat: {
            options: {
                stripBanners: true
            },
            css: {
                files: {
                    'dist/css/libs.css': [
                        'libs/css/bootstrap.css',
                        'libs/css/font-awesome.css'
                    ]
                }
            },
            js: {
                files: {
                    'dist/js/libs.js': [
                        'libs/js/jquery.js',
                        'libs/js/bootstrap.js'
                    ]
                }
            }
        },

        //css minify
        cssmin: {
            dist: {
                files: {
                    'public/libs.min.css': ['dist/css/libs.css']
                }
            }
        },

        //js uglify
        uglify: {
            js: {
                files: {
                    'public/libs.min.js': ['dist/js/libs.js']
                }
            }
        },

        //clean up
        clean: {
            build: {
                src: ['dist/', 'libs/', 'public/lib.min.css', 'public/lib.min.js', 'public/webfonts/']
            }
        }
    });

    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('default', ['clean', 'bowercopy', 'concat', 'cssmin', 'uglify']);
};