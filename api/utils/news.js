const NewsAPI = require('newsapi');
const config = require('../package.json');

exports.newsApi = () => {
    this.api = new NewsAPI(config.apiKey);
    return this.api.v2;
};