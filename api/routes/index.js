var express = require('express');
var router = express.Router();

/* GET headlines. */
router.get('/headlines', function(req, res, next) {
  const { language, category, q, country } = req.query;

  global.newsAPI.topHeadlines({
    category,
    language,
    q,
    country
  }).then(response => {
    res.send(response.articles);
  });
});

/* GET Search. */
router.get('/find', function(req, res, next) {
  const { language, from, q } = req.query;

  global.newsAPI.everything({
    q,
    from,
    to: from,
    language,
    sortBy: 'relevancy'
  }).then(response => {
    res.send(response.articles);
  });
});

module.exports = router;
