import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";

import NavBar from './components/navbar/navbar';
import Tabs from './components/tabs/tabs';
import Loader from './components/loader/loader';
import Layout from './components/layout/layout';

import { loadArticlesIfNone, loadArticles, setDefaultCategory, setDefaultCountry } from './redux/actions'
import { getArticles, isArticlesLoading, getArticlesErrorMessage, getCategories, getCountries, getDefaultCountry } from './redux/selectors';

import './App.css';

class App extends React.PureComponent {
  	componentDidMount () {
    	this.props.loadArticlesIfNone();
 	}

  	changeCategory = (category) => {
    	// set defailt category
    	const cat = category === 'Headlines' ? null : category.toLowerCase(); 
    	this.props.setDefaultCategory(cat);
    
    	// trigger query
    	this.props.loadArticles();
  	};

  	changeCountry = (country) => {
    	// ser default country
    	this.props.setDefaultCountry(country);

    	// trigger query
    	this.props.loadArticles();
  	};
 
  	render() {
    	return (
      		<div className="App">
        		<NavBar countries={this.props.countries} activeCountry={this.props.defaultCountry} set={this.changeCountry} />

        		<Tabs labels={this.props.categories} set={this.changeCategory}/>

				<div className="container-fluid">
				{this.props.isLoading &&
					<Loader />
				}

				{!this.props.isLoading &&
					<Layout articles={this.props.articles} />
				}
				</div>
      		</div>
    	);
  	}
}

App.propTypes = {
  	articles: PropTypes.array,
	errorMsg: PropTypes.string,
	categories: PropTypes.array,
	countries: PropTypes.array,
	defaultCountry: PropTypes.string,
	isLoading: PropTypes.bool,
	loadArticles: PropTypes.func,
	loadArticlesIfNone: PropTypes.func,
	setDefaultCategory: PropTypes.func,
	setDefaultCountry: PropTypes.func
};

const mapStateToProps = state => ({
	articles: getArticles(state),
	categories: getCategories(state),
	countries: getCountries(state),
	defaultCountry: getDefaultCountry(state),
	errorMsg: getArticlesErrorMessage(state),
	isLoading: isArticlesLoading(state)
});
  
const mapDispatchToProps = {
	loadArticles,
	loadArticlesIfNone,
	setDefaultCategory,
	setDefaultCountry
};
  
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App);