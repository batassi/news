import * as types from './constants';
import langs from './data/languages.json';
import cats from './data/categories.json';
import countries from './data/countries.json';

export const initialState = {
    articles: [],
    articlesLoading: false,
    articles_error_message: null,
    languages: langs,
    categories: cats,
    countries,
    defaultCountry: 'us',
    defaultLanguage: 'en',
    defaultCategory: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case types.FETCH_HEADLINES:
            return {
                ...state,
                articlesLoading: true,
                articles_error_message: null
            };

        case types.FETCH_HEADLINES_SUCCESS:
            return {
                ...state,
                articles: action.articles,
                articlesLoading: false
            };

        case types.FETCH_HEADLINES_FAILURE:
            return {
                ...state,
                articles: [],
                articlesLoading: false,
                articles_error_message: action.error
            };

        case types.SET_DEFAULT_LANGUAGE:
            return {
                ...state,
                defaultLanguage: action.language
            };

        case types.SET_DEFAULT_CATEGORY:
            return {
                ...state,
                defaultCategory: action.category
            };

        case types.SET_DEFAULT_COUNTRY:
            return {
                ...state,
                defaultCountry: action.country
            };

        default: 
            return state;
    }
};

export default reducer;