export const getArticles = (state) => state.articles;

export const isArticlesLoading = (state) => state.articlesLoading;

export const getArticlesErrorMessage = (state) => state.articles_error_message;

export const getLanguages = (state) => state.languages;

export const getCategories = (state) => state.categories;

export const getCountries = (state) => state.countries;

export const getDefaultLanguage = (state) => state.defaultLanguage;

export const getDefaultCategory = (state) => state.defaultCategory;

export const getDefaultCountry = (state) => state.defaultCountry;