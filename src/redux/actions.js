import axios from 'axios';
import * as types from './constants';

const getApiPath = () => {
    switch(process.env.REACT_APP_ENV) {
        case 'dev':
            return 'http://https://news-6208.herokuapp.com/api/headlines?';
        case 'local':
            return 'http://localhost:4000/api/headlines?';
        default:
            return 'api/headlines?'
    }
};

const apiPath = getApiPath();

export const loadArticlesIfNone = () => (dispatch, getState) => {
    const state = getState();

    if (state.articles.length === 0) {
        dispatch(loadArticles());
    }
};

export const loadArticles = () => (dispatch, getState) => {
    const state = getState();

    const buildPath = () => {
        var url = apiPath;

        if(state.defaultCountry) {
            url += 'country=' + state.defaultCountry;
        }

        if(state.defaultCountry && state.defaultCategory) {
            url += '&';
        }

        if(state.defaultCategory) {
            url += 'category=' + state.defaultCategory;
        }

        return url;
    };

    dispatch({ type: types.FETCH_HEADLINES });
    axios({
        method: 'get',
        url: buildPath()
    })
    .then(function (response) {
        dispatch({
            type: types.FETCH_HEADLINES_SUCCESS,
            articles: response.data
        });
    })
    .catch(function (error) {
        console.log(error);
        dispatch({
            type: types.FETCH_HEADLINES_FAILURE,
            error
        });
    });
};

export const setDefaultCategory = (category) => (dispatch, getState) => {
    dispatch({
        type: types.SET_DEFAULT_CATEGORY,
        category
    });
};

export const setDefaultLanguage = (language) => (dispatch, getState) => {
    dispatch({
        type: types.SET_DEFAULT_LANGUAGE,
        language
    });
};

export const setDefaultCountry = (country) => (dispatch, getState) => {
    dispatch({
        type: types.SET_DEFAULT_COUNTRY,
        country
    });
};