import React from 'react';
import PropTypes from 'prop-types';

const DEFAULT_IMAGE = '/default_image.jpg';

class Image extends React.PureComponent {
	constructor(props){
		super(props);

		this.state = {
			src: props.src || DEFAULT_IMAGE
		};
	}

	componentDidUpdate (prevProps) {
		if (prevProps.src !== this.props.src) {
			this.setState({
				src: this.props.src
			});
		}
	}

	onError(){
		this.setState({
			src: DEFAULT_IMAGE
		});
	}

  	render() {
    	return(
        	<img src={this.state.src} onError={this.onError.bind(this)} alt="news" />
    	);
  	}
}

Image.propTypes = {
    src: PropTypes.string
}
    
export default Image;