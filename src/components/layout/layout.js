import React from 'react';
import PropTypes from 'prop-types';
import Card from '../card/card';
import './layout.css';

class Layout extends React.PureComponent {
    // rowWidth = 0;
    // maxWidth = 12;

    // computeCardWith = () => {
    //     function randomWidth () {
    //         var r = 0;

    //         while (true) {
    //             r = Math.floor(Math.random() * Math.floor(7));

    //             if (r >= 2 && r < 5) {
    //                 break;
    //             }
    //         }

    //         return r;
    //     }

    //     var cardWidth = randomWidth();

    //     if (this.rowWidth + cardWidth >= this.maxWidth - 1) {
    //         cardWidth = this.maxWidth - this.rowWidth;
    //     }

    //     this.rowWidth += cardWidth;

    //     if (this.rowWidth >= 12) {
    //         this.rowWidth = 0;
    //     }

    //     return cardWidth;
    // };

    generateCards = () => {
        return this.props.articles.map((article, index) =>
            <Card {...article} key={index} width={3}/>
        );
    }

    render () {
        return (
            <div className="row layout">
                {this.generateCards()}
            </div>
        );
    }
}

Layout.propTypes = {
    articles: PropTypes.array
};

export default Layout;