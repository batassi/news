import React from 'react';
import PropTypes from 'prop-types';
import './navbar.css';

class NavBar extends React.PureComponent {
    generateDropdownOptions = () => {
        return this.props.countries.map((country, index) =>
            <option value={country.value} key={index}>{country.text}</option>
        );
    };

    updateCountry = (event) => {
        this.props.set && this.props.set(event.target.value);
    }

    render () {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand" href="/">
                    <i className="far fa-newspaper" />
                    News
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="form-group">
                    <select className="form-control" value={this.props.activeCountry} onChange={this.updateCountry}>
                        {this.generateDropdownOptions()}
                    </select>
                </div>
            </nav>
        );
    }
}

NavBar.propTypes = {
    activeCountry: PropTypes.string,
    countries: PropTypes.array,
    set: PropTypes.func
};

export default NavBar;