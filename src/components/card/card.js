import React from 'react';
import PropTypes from 'prop-types';
import Image from '../image/image';

import './card.css';

class Card extends React.PureComponent {
    parseTitle = () => {
        return this.props.title.substring(0, this.props.title.lastIndexOf('-') - 1);
    }

    parseAuthor = () => {
        if (!this.props.author || this.props.author === this.props.source.name) {
            return 'unknown';
        } else if (this.props.author.indexOf('[') >= 0) {
            const obj = JSON.parse(this.props.author);
            return obj[0].name;
        } else if (this.props.author.indexOf('<a') >= 0) {
            return this.props.author.substring(this.props.author.indexOf('>') + 1, this.props.author.lastIndexOf('</')); 
        } else {
            return this.props.author;
        }
    };

    parseDescription = () => {
        const maxLength = 110;
        var descr = this.props.description;
        if (!this.props.description || this.props.description === '') {
            descr = this.props.content ? this.props.content.substring(0, this.props.content.lastIndexOf('[')) : '';
        }

        if(descr.length > maxLength) {
            descr = descr.substring(0, maxLength) + '...';
        }

        return descr;
    }

    redirectToUrl = () => {
        window.open(this.props.url, '_blank');
    };

    render () {
        return (
            <div className={"col-md-" + this.props.width}>
                <div className="card">
                    <h5 className="card-title" onClick={() => this.redirectToUrl()}>{this.parseTitle()}</h5>
                    <div className="card-info">
                        <span className="card-source">{this.props.source.name}</span>
                        <span className="card-date">{new Date(this.props.publishedAt).toLocaleString()}</span>
                        <br />
                        <span className="card-author">{this.parseAuthor()}</span>
                    </div>
                    <div className="card-image">
                        <Image src={this.props.urlToImage} />
                    </div>
                    <div className="card-description">{this.parseDescription()}</div>
                </div>
            </div>
        );
    }
}

Card.propTypes = {
    source: PropTypes.shape({
        id: PropTypes.any,
        name: PropTypes.string
    }),
    author: PropTypes.any,
    title: PropTypes.string,
    description: PropTypes.string,
    url: PropTypes.string,
    urlToImage: PropTypes.string,
    publishedAt: PropTypes.string,
    content: PropTypes.any,
    width: PropTypes.number,
};

export default Card;