import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap-modal';
import './reader.css';

class Reader extends React.PureComponent {
    close = () => {

    };

    render () {
        if (!this.props.url) {
            return null;
        }

        return (
            <Modal show={true} onHide={this.close} aria-labelledby="ModalHeader">
                <Modal.Header closeButton>
                    <Modal.Title id="ModalHeader">{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <iframe src={this.props.url} title="article reader"/>
                </Modal.Body>
            </Modal>
        );
    }
}

Reader.propTypes = {
    title: PropTypes.string,
    url: PropTypes.string
};

export default Reader;