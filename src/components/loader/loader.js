import React from 'react';
import './loader.css';

class Loader extends React.PureComponent {
    render () {
        return (
            <div className="loader">
                <i className="fas fa-circle-notch fa-spin fa-7x" />
            </div>
        );
    }
}

export default Loader;