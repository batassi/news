import React from 'react';
import PropTypes from 'prop-types';
import './tabs.css';

class Tabs extends React.PureComponent {
    constructor (props) {
        super(props);

        this.state = {
            activeIndex: 0
        };
    }

    setActive = (index) => {
        this.setState({ activeIndex: index });
        this.props.set && this.props.set(this.props.labels[index]);
    }

    generateTabs = () => {
        return this.props.labels.map((label, index) =>
            <li className="nav-item" key={index}>
                <a className={"nav-link" + (index === this.state.activeIndex ? ' active': '')} href="#" onClick={() => this.setActive(index)}>{label}</a>
            </li>
        );
    };

    render () {
        return (
            <ul className="nav nav-pills nav-fill tabs">
                {this.generateTabs()}
            </ul>
        );
    }
}

Tabs.propTypes = {
    labels: PropTypes.array,
    set: PropTypes.func
};

export default Tabs;